<?php
function ubah_huruf($string){
	//kode di sini
	$hasil = "";
	for($i = 0; $i < strlen($string); $i++){
		if ($string[$i] == 'z') {
			$next_str = 'a';
		} else if ($string[$i] == 'Z') {
			$next_str = 'A';
		} else{
			$next_str = chr(ord($string[$i])+1);
		}
		$hasil .= $next_str;
	}
	return $hasil;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo "<br>";
echo ubah_huruf('developer'); // efwfmpqfs
echo "<br>";
echo ubah_huruf('laravel'); // mbsbwfm
echo "<br>";
echo ubah_huruf('keren'); // lfsfo
echo "<br>";
echo ubah_huruf('semangat'); // tfnbohbu

?>